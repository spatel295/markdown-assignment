#Once *Connected*, Always *Connected*

>You cannot opt out of the network completely, but at the very least you can try to be connected a little less. You can regulate your own contributions to the system that is regulating you. (Steven Shaviro 5)
************

The word *connected* or *connection* is often associated with the relationship between two people. However, what is misunderstood is the fact that people are connected to nonhuman things, even deeper than they are to humans. Different forms of technologies, such as a computer or phones, are a necessity for people, carrying out all their work on devices and programs that originally seem simple. However, this dependence makes them more *connected* to algorithms and code than they think. 

Markdown allows us to have this *connection* with our technological devices. It creates a code or algorithm for us that simplifies the way in which we **create** and **see** by doing the work for us. It acts as a *middle man* between the human and the work that is produced by the human.We are often caught looking more into the final presentation of our projects rather than the *content* we are trying to get across. Word processors such as *Word* or *Google Docs* disguise the use of Markdown by complicating the look of words by adding a wide variety of fonts, or by giving the option of *italics* or **bold**. 

Markdown wants simplicity whereas word processors want [complexity](https://designschool.canva.com/font-design/). Shaviro says,"A pattern of information is meaningless by itself. A virus remains inert unless it encounters a suitable living cell. A configuration of ones and zeros is similarly no more than gibberish until it is processed by the right program" (Shaviro 16). This same concept applies to Markdown. People see the format of Markdown and instantly feel overwhelmed and intimidated. The use of characters to represent headers, bold or italics is an otherwise foreign concept. It can be used as a tool to *compose* but requires more work as it requires you to do ***all*** the composing. Every detail must be put in by the composer. Therefore, it can be seen as meaningless and complicated when in reality it is the simplest form of text. Until Markdown is transferred into a Word Processor or website, it has no real value to the composer because it lacks the layout and design. 

![Markdown vs. Word](https://www.google.com/search?q=markdown+vs+word&rlz=1C5CHFA_enUS720US720&tbm=isch&source=lnms&sa=X&ved=0ahUKEwiNlrGHn63WAhUs9YMKHQuTBroQ_AUICygC&biw=1280&bih=672&dpr=2#imgrc=YBgSomOCzhXeLM:)

The simplicity of Markdown lacks the direct embellishments that are present in word processors. Shaviro writes, "Everything is code, or specific expression" (47). Markdown, ultimately, is the code present behind the specific fonts and designs of word processing. It allows character representations to come to life on paper, changing the way people feel and perceive the words written. If Markdown were not present, composers would lack the ability to [visually captivate](https://www.smashingmagazine.com/2012/04/when-typography-speaks-louder-than-words/) the readers attention with big headers or different fonts. 

Steven Shaviro says, "You cannot opt out of the network completely, but at the very least you can try to be connected a little less. You can regulate your own contributions to the system that is regulating you"(5). Once there has been a dependency on word processors, it is hard to revert to a more straightforward medium of composing. However, regardless of composing in Markdown or in a word processor, there is no decline in the connectedness to the network. This is because Markdown itself, is a way to connect us to the network. While it may not be the final product, it allows us to *reach* the final product.

![markdown gif](https://media.giphy.com/media/Z6QNUBLkx1RGU/giphy.gif)

####Author
Sonia Patel